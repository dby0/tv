/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.sky;

import junit.framework.Assert;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.w3c.dom.Document;

import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 31/03/13
 */
public class TestParser {

    @Test
    public void testParser() throws Exception {
        ChannelNameTranslator translator = new ChannelNameTranslator();
        ResultsParser parser = new ResultsParser();
        parser.setChannelNameTranslator(translator);
        Document document = DOMUtilities.getDocumentFromStringContent(IOUtils.toString(this.getClass().getResourceAsStream("/exampleskyresponse-unescaped.xml")));
        List<SkyRecording> recordings = parser.parseXml(document);
        Assert.assertEquals(1, recordings.size());
    }
}
