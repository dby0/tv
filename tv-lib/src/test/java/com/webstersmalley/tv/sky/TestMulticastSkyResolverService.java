package com.webstersmalley.tv.sky;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

import static org.junit.Assert.*;

/**
 * Created by: Matthew Smalley
 * Date: 24/08/13
 */
public class TestMulticastSkyResolverService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testIt() {
        try {
            String descriptionXml = IOUtils.toString(this.getClass().getResourceAsStream("/example-upnp-response.xml"));
            MulticastSkyResolverService service = new MulticastSkyResolverService();
            URL url = service.getURLFromDescription(descriptionXml);
            assertEquals(url, new URL("http://localhost:49153/THEUUIDSkyBrowse"));
        } catch (Exception e) {
            logger.error("Exception thrown: ", e);
            fail();
        }

    }
}
