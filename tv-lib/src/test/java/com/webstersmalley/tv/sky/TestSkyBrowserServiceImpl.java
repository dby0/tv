/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.sky;

import junit.framework.Assert;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.IOException;

/**
 * Created by: Matthew Smalley
 * Date: 15/04/13
 */
public class TestSkyBrowserServiceImpl {

    @Test
    public void testGetContents() throws IOException {
        String fullContents = IOUtils.toString(this.getClass().getResourceAsStream("/example-sky-response-fullhttp.txt"));
        fullContents = fullContents.substring(fullContents.indexOf("<s:Envelope"));
        SkyBrowserServiceImpl sky = new SkyBrowserServiceImpl();

        Document document = sky.getDocumentFromHttpResonse(fullContents);
        Assert.assertEquals(25, sky.getTotalMatches(document));


    }
}
