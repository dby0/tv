/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.sky;

import com.webstersmalley.tv.AbstractSpringEnabledTests;
import com.webstersmalley.tv.db.TableDumper;
import com.webstersmalley.tv.db.TvDao;
import com.webstersmalley.tv.service.ListingsRefresher;
import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;

/**
 * Created by: Matthew Smalley
 * Date: 21/04/13
 */
public class TestSkyService extends AbstractSpringEnabledTests {
    @Resource(name = "tvDao")
    private TvDao tvDao;

    @Resource(name = "skyService")
    private SkyService skyService;

    @Resource(name = "tableDumper")
    private TableDumper tableDumper;

    @Resource(name = "listingsRefresher")
    private ListingsRefresher listingsRefresher;

    @Test
    public void testImportRecordings() {
        listingsRefresher.refreshListings();


        assertEquals(0, tvDao.getAllRecordings().size());
        skyService.importRecordings();
        assertEquals(1, tvDao.getAllRecordings().size());

    }


}
