/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.db;

import com.webstersmalley.tv.AbstractSpringEnabledTests;
import com.webstersmalley.tv.domain.SearchQuery;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 23/09/12
 */
public class TestSearchQueryDao extends AbstractSpringEnabledTests {
    @Resource(name = "searchQueryDao")
    private SearchQueryDao searchQueryDao;

    @Test
    public void testSaveNoKeywords() {
        SearchQuery searchQuery = new SearchQuery();
        searchQuery.setEpisodeNumber("1");
        searchQueryDao.saveSearchQuery(searchQuery);
    }

    @Test
    public void testSaveNoParameters() {
        SearchQuery searchQuery = new SearchQuery();
        try {
            searchQueryDao.saveSearchQuery(searchQuery);
            Assert.fail();
        } catch (RuntimeException e) {
            Assert.assertTrue(e.getMessage().contains("parameters"));
        }

    }

    @Test
    public void testSaveAndGet() {
        SearchQuery searchQuery = new SearchQuery();
        searchQuery.setEpisodeNumber("1");
        searchQuery.setSeasonNumber("10");
        List<String> keywords = new ArrayList<String>();
        keywords.add("word1");
        keywords.add("word2");
        searchQuery.setKeywords(keywords);
        searchQuery.setTitleSearch("search2");

        List<SearchQuery> initialSearchQueryList = searchQueryDao.getSearchQueries();
        Assert.assertTrue(!initialSearchQueryList.contains(searchQuery));

        searchQueryDao.saveSearchQuery(searchQuery);

        List<SearchQuery> afterSaveSearchQueryList = searchQueryDao.getSearchQueries();
        Assert.assertTrue(afterSaveSearchQueryList.contains(searchQuery));

        Assert.assertTrue(afterSaveSearchQueryList.size() == initialSearchQueryList.size() + 1);
    }
}
