/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.domain;

import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by: Matthew Smalley
 * Date: 15/07/12
 */
public class TestProgram {

    private void checkKeywords(String title, String[] expectedEntries) {
        Program program = new Program();
        program.setTitle(title);

        Set<String> keywords = program.getKeywords();

        assertEquals(expectedEntries.length, keywords.size());
        for (String entry : expectedEntries) {
            assertTrue("Expected to find: " + entry, keywords.contains(entry));
        }
    }

    @Test
    public void testStandardKeywords() {
        checkKeywords("This is the title", new String[]{"this", "is", "the", "title"});
    }

    @Test
    public void testDuplicates() {
        checkKeywords("One one ONE onE one one one", new String[]{"one"});
    }

    @Test
    public void testNonAlphaCharacters() {
        checkKeywords("O-N-E, o:ne (one)", new String[]{"one"});
    }
}
