/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.AbstractSpringEnabledTests;
import com.webstersmalley.tv.domain.Channel;
import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.domain.SearchQuery;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Matthew Smalley
 */
public class TestListerService extends AbstractSpringEnabledTests {

    @Resource(name = "listerService")
    private ListerService listerService;

    @Resource(name = "listingsRefresher")
    private ListingsRefresher listingsRefresher;

    private JdbcTemplate jdbcTemplate;

    @Resource(name = "dataSource")
    private void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Before
    public void setupListings() {
        jdbcTemplate.execute("delete from programs");
        listingsRefresher.refreshListings();
    }

    @Test
    public void testListProgramsTrue() {
        List<Program> results;

        results = listerService.getProgramsByTitle("ProgramTitle");
        assertTrue(results.size() > 0);
    }

    @Test
    public void testListProgramsFalse() {
        List<Program> results;

        results = listerService.getProgramsByTitle("Bobcobob");
        assertTrue(results == null || results.size() == 0);
    }

    @Test
    public void testListProgramsByNameAndChannelTrue() {
        List<Program> results;

        results = listerService.getProgramsByTitleAndChannel("ProgramTitle", "Channel 1");
        assertTrue(results.size() > 0);
    }

    @Test
    public void testListProgramsByNameAndChannelFalse() {
        List<Program> results;

        results = listerService.getProgramsByTitleAndChannel("DuffTitle", "Channel 1");
        assertTrue(results == null || results.size() == 0);

        results = listerService.getProgramsByTitleAndChannel("ProgramTitle", "Channel 53");
        assertTrue(results == null || results.size() == 0);

        results = listerService.getProgramsByTitleAndChannel("ProgramTitle", "Channel 1");
        assertTrue(results != null && results.size() > 0);

        Program program = results.get(0);
        assertNotNull(program.getTitle());
        assertNotNull(program.getSubtitle());
        assertNotNull(program.getStartTime());
        assertTrue(program.getDuration() > 0);
    }

    @Ignore("Temporarily removed keywords")
    @Test
    public void testSearchProgramsBySearch() {
        SearchQuery query = new SearchQuery();
        query.setKeywords(Collections.singletonList("news"));
        assertTrue(listerService.getProgramsBySearchQuery(query).size() > 0);
    }

    @Test
    public void testSearchProgramsByBlankQuery() {
        SearchQuery query = new SearchQuery();
        assertTrue(listerService.getProgramsBySearchQuery(query).size() > 0);
    }

    @Test
    public void testSearchProgramsByTitle() {
        SearchQuery query = new SearchQuery();
        query.setTitleSearch("%Program%");

        List<Program> listByTitle = listerService.getProgramsBySearchQuery(query);

        assertTrue(listByTitle.size() > 0);

    }

    @Ignore("Temporarily removed keywords")
    @Test
    public void testSearchProgramsByKeywords() {
        SearchQuery query = new SearchQuery();
        query.setKeywords(Collections.singletonList("khoo"));
        List<Program> listBySearchString = listerService.getProgramsBySearchQuery(query);

        assertTrue(listBySearchString.size() > 0);
    }

    @Test
    public void testListProgramsByChannelTrue() {
        List<Program> results;
        Channel bbc1 = listerService.getChannel("Channel 1");
        results = listerService.getProgramsByChannel(bbc1);
        assertTrue(results.size() > 0);

    }

    @Test
    public void testListChannels() {
        List<Channel> channels = listerService.getChannels();
        assertTrue(channels.size() > 0);
    }

    @Test
    public void testGetChannel() {
        Channel channel = listerService.getChannel("Channel 1");
        assertNotNull(channel);
        try {
            channel = listerService.getChannel("Channel 53");
            fail();
        } catch (EmptyResultDataAccessException e) {

        }
    }

    @Test
    public void testGetChannelById() {
        int validChannelId = 1;
        String validChannelName = "Channel 1";
        int bogusChannelId = -54;

        Channel channel = listerService.getChannelById(validChannelId);
        assertEquals(channel.getName(), validChannelName);

        try {
            channel = listerService.getChannelById(bogusChannelId);
            fail();
        } catch (EmptyResultDataAccessException e) {

        }
    }
}
