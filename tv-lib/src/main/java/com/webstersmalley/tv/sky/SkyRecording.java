/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.sky;

import com.webstersmalley.tv.domain.Recording;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by: Matthew Smalley
 * Date: 14/04/13
 */
public class SkyRecording extends Recording {
    private String channelNr;


    public String getChannelNr() {
        return channelNr;
    }

    public void setChannelNr(String channelNr) {
        this.channelNr = channelNr;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
