/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.sky;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 14/04/13
 */
@Service("resultsParser")
public class ResultsParser {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private final DateTimeFormatter dtf = ISODateTimeFormat.dateTimeParser();

    @Resource(name = "channelNameTranslator")
    private ChannelNameTranslator channelNameTranslator;

    public void setChannelNameTranslator(ChannelNameTranslator channelNameTranslator) {
        this.channelNameTranslator = channelNameTranslator;
    }

    private String filterTitle(String input) {
        if (input == null) {
            return null;
        }
        String output = input;

        if (output.startsWith("New: ")) {
            output = output.replaceFirst("New: ","");
        }

        if (output.startsWith("New ")) {
            output = output.replaceFirst("New ","");
        }
        return output.replaceAll("[^A-Za-z0-9 ]","").toUpperCase();
    }

    public List<SkyRecording> parseXml(Document document) {
        List<SkyRecording> recordings = new ArrayList<SkyRecording>();
        NodeList nodes = document.getElementsByTagName("item");
        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);
            SkyRecording recording = new SkyRecording();
            recording.setTitle(filterTitle(DOMUtilities.getElementText(element, "dc:title")));
            String startTime = DOMUtilities.getElementText(element, "upnp:scheduledStartTime");
            if (startTime != null) {
                recording.setStartTime(dtf.parseDateTime(startTime));
            }
            String endTime = DOMUtilities.getElementText(element, "upnp:scheduledEndTime");
            if (endTime != null) {
                recording.setEndTime(dtf.parseDateTime(endTime));
            }
            recording.setChannelNr(DOMUtilities.getElementText(element, "upnp:channelNr"));
            String originalChannelName = DOMUtilities.getElementText(element, "upnp:channelName");
            if (originalChannelName.endsWith("+1")) {
                recording.setStartTime(recording.getStartTime().minusHours(1));
                recording.setEndTime(recording.getEndTime().minusHours(1));
                originalChannelName = originalChannelName.substring(0, originalChannelName.lastIndexOf("+1")).trim();
            }
            recording.setChannelName(channelNameTranslator.getChannelName(originalChannelName));
            recording.setEpisodeNumber(DOMUtilities.getElementText(element, "upnp:episodeNumber"));
            recording.setSeasonNumber(DOMUtilities.getElementText(element, "vx:X_seasonNumber"));
            recording.setRecStatus(DOMUtilities.getElementText(element, "vx:X_recStatus"));
            recordings.add(recording);
        }

        return recordings;
    }

}
