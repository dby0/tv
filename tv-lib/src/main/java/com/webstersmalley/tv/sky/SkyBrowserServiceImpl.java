/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.sky;

import com.webstersmalley.tv.comms.Comms;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import javax.annotation.Resource;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 31/03/13
 */
@Service("skyBrowserService")
public class SkyBrowserServiceImpl implements SkyBrowserService {

    @Resource(name = "skyResolverService")
    private SkyResolverService skyResolverService;
    private String requestTemplate;
    private Logger logger = LoggerFactory.getLogger(getClass());
    private int requestedCount = 25;

    private URL url;

    @Resource(name = "comms")
    private Comms comms;

    public void setSkyResolverService(SkyResolverService skyResolverService) {
        this.skyResolverService = skyResolverService;
    }

    @Value("${sky.requesttemplate}")
    public void setRequestTemplatePath(String path) {
        try {
            requestTemplate = IOUtils.toString(getClass().getResourceAsStream(path));
        } catch (Exception e) {
            logger.error("Error reading template: " + path, e);
            throw new RuntimeException("Error reading template: " + path, e);
        }
    }

    @Resource(name = "resultsParser")
    private ResultsParser resultsParser;

    private String buildRequest(URL url, int requestedCount, int startingIndex) {
        String contents = requestTemplate.replaceAll("@@REQUESTEDCOUNT@@", Integer.valueOf(requestedCount).toString()).replaceAll("@@STARTINGINDEX@@", Integer.valueOf(startingIndex).toString());
        StringBuilder sb = new StringBuilder();

        sb.append("POST " + url.toString() + " HTTP/1.1\r\n" +
                "Host: " + url.getHost() + ":" + url.getPort() + "\r\n" +
                "Connection: keep-alive\r\n" +
                "Accept: */*\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Accept-Language: en-us\r\n" +
                "Content-Type: text/xml; charset=utf-8\r\n" +
                "Pragma: no-cache\r\n" +
                "SOAPACTION: \"urn:schemas-nds-com:service:SkyBrowse:2#Browse\"\r\n" +
                "Content-Length: ").append(contents.length()).append("\r\n").append(
                "Connection: keep-alive\r\n" +
                "User-Agent: SKY_skyplus\r\n\r\n");
        sb.append(contents);
        return sb.toString();
    }

    private String getResults(String content) throws IOException {
        return StringEscapeUtils.unescapeXml(content.substring(content.indexOf("<Result>") + "<Result>".length(), content.indexOf("</Result>")));
    }

    protected Document getDocumentFromHttpResonse(String httpResponse) {
        return DOMUtilities.getDocumentFromStringContent(httpResponse.toString());
    }

    protected int getTotalMatches(Document document) {
        return new Integer(DOMUtilities.getElementText(document, "TotalMatches"));
    }

    @Override
    public List<SkyRecording> getListOfRecordings() {
        if (url == null) {
            url = skyResolverService.getSkyURL();
        }
        int totalMatches = 1000;
        List<SkyRecording> recordings = new ArrayList<SkyRecording>();
        for (int i = 0; i < totalMatches; i = i + requestedCount) {
            String request = buildRequest(url, requestedCount, i);
            String response = comms.getResourceByRawHttp(url, request);
            System.out.println(request + "\n" + response);
            response = response.substring(response.indexOf("<s:Envelope"));
            Document document = getDocumentFromHttpResonse(response);
            totalMatches = getTotalMatches(document);
            Document results = DOMUtilities.getDocumentFromStringContent(DOMUtilities.getElementText(document, "Result"));
            recordings.addAll(resultsParser.parseXml(results));
        }
        for (SkyRecording recording: recordings) {
            logger.info(recording.toString());
        }

        return recordings;

    }


}
