/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Program {
    private int id = -1;
    private int weekNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private Channel channel;
    private String title;
    private String subtitle;
    private String description;
    private String seasonNumber;
    private String episodeNumber;
    private String searchTitle;
    private boolean repeat;
    private boolean newSeries;
    private boolean movie;
    private String year;

    private DateTime startTime;
    private int duration;

    private boolean hasNameMatchedRecordings;
    private boolean hasNameAndChannelMatchedRecordings;
    private boolean hasExactMatchRecordings;

    private String imdbId;
    private BigDecimal imdbRating;
    private String imdbSource;

    private List<String> tags;

    public List<String> getTags() {
        return tags;
    }

    public void calculateTags() {
        tags = new ArrayList<String>();
        DateTime now = new DateTime();
        if (hasExactMatchRecordings) {
            if (startTime.isBefore(now)) {
                tags.add("RECORDED");
            } else {
                tags.add("SCHEDULED");
            }
        }
    }

    public boolean isHasNameMatchedRecordings() {
        return hasNameMatchedRecordings;
    }

    public void setHasNameMatchedRecordings(boolean hasNameMatchedRecordings) {
        this.hasNameMatchedRecordings = hasNameMatchedRecordings;
    }

    public boolean isHasNameAndChannelMatchedRecordings() {
        return hasNameAndChannelMatchedRecordings;
    }

    public void setHasNameAndChannelMatchedRecordings(boolean hasNameAndChannelMatchedRecordings) {
        this.hasNameAndChannelMatchedRecordings = hasNameAndChannelMatchedRecordings;
    }

    public boolean isHasExactMatchRecordings() {
        return hasExactMatchRecordings;
    }

    public void setHasExactMatchRecordings(boolean hasExactMatchRecordings) {
        this.hasExactMatchRecordings = hasExactMatchRecordings;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(String seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public String getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(String episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public final String getTitle() {
        return title;
    }

    public final void setTitle(String title) {
        this.title = title;
        this.searchTitle = title.toUpperCase().replaceAll("[^A-Za-z0-9 ]", "");
    }

    public final String getSubtitle() {
        return subtitle;
    }

    public final void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public final DateTime getStartTime() {
        return startTime;
    }

    public final void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public String getTitleAndChannelKey() {
        return channel.toString() + ":" + title;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }
        if (object.getClass() != getClass()) {
            return false;
        }
        Program rhs = (Program) object;
        return new EqualsBuilder()
                .append(channel, rhs.channel)
                .append(startTime, rhs.startTime)
                .append(title, rhs.title)
                .append(duration, rhs.duration)
                .isEquals();
    }

    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 31);

        return hcb
                .append(channel)
                .append(startTime)
                .append(title)
                .append(duration)
                .toHashCode();
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this).toString();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEpisodeNumberText() {
        StringBuilder sb = new StringBuilder();
        if (seasonNumber != null) {
            sb.append("S").append(seasonNumber);
        }
        if (episodeNumber != null) {
            sb.append("E").append(episodeNumber);
        }
        return sb.toString();
    }

    public Set<String> getKeywords() {
        Set<String> keywords = new HashSet<String>();
        if (title != null) {
            for (String keyword : title.split(" ")) {
                keywords.add(keyword.replaceAll("[^A-Za-z0-9]", "").toLowerCase());
            }
        }

        return keywords;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public boolean isNewSeries() {
        return newSeries;
    }

    public void setNewSeries(boolean newSeries) {
        this.newSeries = newSeries;
    }

    public String getSearchTitle() {
        return searchTitle;
    }

    public boolean isMovie() {
        return movie;
    }

    public void setMovie(boolean movie) {
        this.movie = movie;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public BigDecimal getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(BigDecimal imdbRating) {
        this.imdbRating = imdbRating;
    }

    public String getImdbSource() {
        return imdbSource;
    }

    public void setImdbSource(String imdbSource) {
        this.imdbSource = imdbSource;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getWeekNumber() {
        return weekNumber;
    }
}
