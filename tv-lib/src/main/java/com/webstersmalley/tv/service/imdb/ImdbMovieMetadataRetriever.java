package com.webstersmalley.tv.service.imdb;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webstersmalley.tv.comms.Comms;
import com.webstersmalley.tv.domain.Program;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import static com.webstersmalley.tv.service.imdb.ImdbUtils.encode;

/**
 * Created by: Matthew Smalley
 * Date: 13/05/13
 */
@Service("imdbApiImdbDataRetriever")
public class ImdbMovieMetadataRetriever implements ImdbDataRetriever {
    private final static String SOURCE = "imdbapi.org";
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource(name = "longtimeoutComms")
    private Comms comms;

    private ObjectMapper mapper = new ObjectMapper();

    public ImdbMovieMetadataRetriever() {
        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    private MovieMetadata getMovieMetadata(Program program) {
        try {
            String query = "http://imdbapi.org/?q=" + encode(program.getTitle());
            if (program.getYear() != null) {
                query = query + "&year=" + program.getYear() + "&yg=1";
            }
            String response = comms.getURLAsString(query);
            if (response.contains("Film not found")) {
                if (program.getYear() == null) {
                    logger.debug("Didn't find metadata for {}, giving up", program.getTitle());
                    return null;
                }
                logger.debug("Didn't find metadata for {} {}, trying one up", program.getTitle(), program.getYear());
                query = "http://imdbapi.org/?q=" + encode(program.getTitle()) + "&year=" + (Integer.valueOf(program.getYear())+1) + "&yg=1";
                response = comms.getURLAsString(query);
                if (response.contains("Film not found")) {
                    logger.debug("Didn't find metadata for {} {}, trying one down", program.getTitle(), program.getYear());
                    query = "http://imdbapi.org/?q=" + encode(program.getTitle()) + "&year=" + (Integer.valueOf(program.getYear())-1) + "&yg=1";
                    response = comms.getURLAsString(query);
                    if (response.contains("Film not found")) {
                        logger.debug("Didn't find metadata for {} {}, one up or down, giving up", program.getTitle(), program.getYear());
                        return null;
                    }
                }
            }
            List<MovieMetadata> movieMetadatas = mapper.readValue(response, new TypeReference<List<MovieMetadata>>() { });
            return  movieMetadatas.get(0);
        } catch (IOException e) {
            throw new RuntimeException("Error parsing metadata: " + e.getMessage(), e);
        }
    }

    @Override
    public void addImdbData(Program program) {
        try {
            String query = "http://imdbapi.org/?q=" + encode(program.getTitle());
            if (program.getYear() != null) {
                query = query + "&year=" + program.getYear() + "&yg=1";
            }
            String response = comms.getURLAsString(query);
            if (response.contains("Film not found")) {
                if (program.getYear() == null) {
                    logger.debug("Didn't find metadata for {}, giving up", program.getTitle());
                    return;
                }
                logger.debug("Didn't find metadata for {} {}, trying one up", program.getTitle(), program.getYear());
                query = "http://imdbapi.org/?q=" + encode(program.getTitle()) + "&year=" + (Integer.valueOf(program.getYear())+1) + "&yg=1";
                response = comms.getURLAsString(query);
                if (response.contains("Film not found")) {
                    logger.debug("Didn't find metadata for {} {}, trying one down", program.getTitle(), program.getYear());
                    query = "http://imdbapi.org/?q=" + encode(program.getTitle()) + "&year=" + (Integer.valueOf(program.getYear())-1) + "&yg=1";
                    response = comms.getURLAsString(query);
                    if (response.contains("Film not found")) {
                        logger.debug("Didn't find metadata for {} {}, one up or down, giving up", program.getTitle(), program.getYear());
                        return;
                    }
                }
            }
            List<MovieMetadata> movieMetadatas = mapper.readValue(response, new TypeReference<List<MovieMetadata>>() { });
            program.setImdbId(movieMetadatas.get(0).getImdb_id());
            program.setImdbRating(movieMetadatas.get(0).getRating());
            program.setImdbSource(SOURCE);
        } catch (IOException e) {
            throw new RuntimeException("Error parsing metadata: " + e.getMessage(), e);
        }
    }
}
