/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.comms.IOHelper;
import com.webstersmalley.tv.db.TvDao;
import com.webstersmalley.tv.domain.Channel;
import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.service.imdb.ImdbDataRetriever;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by: Matthew Smalley
 * Date: 28/04/12
 */
@Service("listingsRefresher")
public class ListingsRefresher {

    @Resource(name = "tvDao")
    private TvDao tvDao;

    @Resource(name = "listingsProviderService")
    private ListingsProviderService listingsProviderService;
    private List<String> channels;

    @Resource(name = "imdbDataRetriever")
    private ImdbDataRetriever imdbDataRetriever;

    public void setChannelListFilename(String channelListFilename) {
        channels = IOHelper.getLines(channelListFilename);
    }

    public void setChannels(List<String> channels) {
        this.channels = channels;
    }

    private void populateChannelData() {
        for (Channel channel : listingsProviderService.getChannels()) {
            tvDao.addChannel(channel);
            tvDao.clearAllPrograms(channel);
        }
    }

    private int threads = 4;

    private void populateProgramData(List<String> channels) {
        ExecutorService executor = Executors.newFixedThreadPool(threads);
        for (final Channel channel : tvDao.getChannels()) {
            if (channels.contains(channel.getName().trim())) {
                executor.execute(new Runnable() {
                    public void run() {
                        populateProgramData(channel);
                    }
                });
            }
        }
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException("Error waiting for tasks to complete: " + e.getMessage(), e);
        }
    }

    private void populateProgramData(Channel channel) {
        Set<Program> programs = listingsProviderService.getProgramsForChannel(channel);
        for (Program program : programs) {
            if (program.isMovie() && imdbDataRetriever != null) {
                //imdbDataRetriever.addImdbData(program);
            }
            tvDao.addProgram(program);
        }
    }

    public void refreshListings() {
        populateChannelData();
        populateProgramData(channels);
    }

    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("environment.xml");
        ListingsRefresher listingsRefresher = ac.getBean("listingsRefresher", ListingsRefresher.class);
        listingsRefresher.refreshListings();
    }
}
