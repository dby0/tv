/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.domain.SearchQuery;
import org.apache.velocity.app.VelocityEngine;
import org.joda.time.DateTime;
import org.springframework.ui.velocity.VelocityEngineUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: Matthew Smalley
 * Date: 06/05/13
 */
public abstract class AbstractSearchResultsHandler implements SearchResultsHandler {
    private VelocityEngine velocityEngine;

    public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }

    protected String getSearchResultsHtmlContent(final Map<SearchQuery, List<Program>> programs, List<Program> movies, List<Program> summaries) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("results", programs);
        model.put("oneWeekInTheFuture", new DateTime().plusDays(7));
        List<SearchQuery> searchQueries = new ArrayList<SearchQuery>();
        searchQueries.addAll(programs.keySet());
        Collections.sort(searchQueries);
        model.put("searchQueries", searchQueries);
        model.put("movies", movies);
        model.put("summaries", summaries);
        return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "searchresults.vm", model);
    }
}
