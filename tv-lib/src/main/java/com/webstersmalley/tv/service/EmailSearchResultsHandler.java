/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.domain.SearchQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Map;

/**
 * Created by: Matthew Smalley
 * Date: 23/09/12
 */
public class EmailSearchResultsHandler extends AbstractSearchResultsHandler {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private JavaMailSender mailSender;
    private String recipients;
    private String from;
    private String subject;

    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public void handleResults(final Map<SearchQuery, List<Program>> programs, final List<Program> movies, final List<Program> summaries) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
                if (recipients.contains(";")) {
                    message.setTo(recipients.split(";"));
                } else {
                    message.setTo(recipients);
                }
                message.setFrom(from);
                message.setSubject(subject);

                message.setText(getSearchResultsHtmlContent(programs, movies, summaries), true);
            }
        };
        try {
            logger.info("Sending email");
            this.mailSender.send(preparator);
        } catch (Exception e) {
            logger.error("Error sending email: " + e.getMessage(), e);
            throw new RuntimeException("Error sending email: " + e.getMessage(), e);
        }
    }
}
