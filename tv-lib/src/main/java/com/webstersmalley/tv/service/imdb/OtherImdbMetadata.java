package com.webstersmalley.tv.service.imdb;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;

/**
 * Created by: Matthew Smalley
 * Date: 17/05/13
 */
public class OtherImdbMetadata {
    private String imdbID;
    private String imdbRating;

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
