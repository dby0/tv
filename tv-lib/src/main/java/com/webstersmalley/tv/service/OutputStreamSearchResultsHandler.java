/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.domain.SearchQuery;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * Created by: Matthew Smalley
 * Date: 20/01/13
 */
@Service("searchResultsHandlerx")
public class OutputStreamSearchResultsHandler extends AbstractSearchResultsHandler {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private String filename;
    private OutputStream os;

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setOs(OutputStream os) {
        this.os = os;
    }

    @Override
    public void handleResults(Map<SearchQuery, List<Program>> programs, List<Program> movies, List<Program> summaries) {
        String results = getSearchResultsHtmlContent(programs, movies, summaries);
        try {
            if (filename != null) {
                os = new FileOutputStream(filename);
            }
            if (os == null) {
                System.out.println(results);
            } else {
                IOUtils.write(results, os);
                IOUtils.closeQuietly(os);
            }
        } catch (IOException e) {
            logger.error("Error writing results: " + e.getMessage(), e);
            throw new RuntimeException("Error writing results: " + e.getMessage(), e);
        }
    }
}
