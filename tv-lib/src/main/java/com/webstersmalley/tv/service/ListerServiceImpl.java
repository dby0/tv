/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.db.TvDao;
import com.webstersmalley.tv.domain.Channel;
import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.domain.SearchQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("listerService")
public class ListerServiceImpl implements ListerService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TvDao tvDao;

    @Override
    public Channel getChannel(String channelName) {
        return tvDao.getChannel(channelName);
    }

    @Override
    public Channel getChannelById(int channelId) {
        return tvDao.getChannelById(channelId);
    }

    @Override
    public List<Program> getProgramsByTitle(String programTitle) {
        return tvDao.getProgramsByTitle(programTitle);
    }

    @Override
    public List<Program> getProgramsBySearchQuery(SearchQuery searchQuery) {
        logger.info("Searching: {}", searchQuery);
        return tvDao.getProgramsBySearchQuery(searchQuery);
    }

    @Override
    public List<Program> getProgramsBySimpleSearchQuery(String query) {
        String seasonNumber = null;
        String episodeNumber = null;
        List<String> keywords = new ArrayList<String>();

        for (String bit : query.split(" ")) {
            if (bit.startsWith("S")) {
                seasonNumber = bit.replace("S", "");
            } else if (bit.startsWith("E")) {
                episodeNumber = bit.replace("E", "");
            } else {
                keywords.add(bit.toLowerCase().trim());
            }
        }
        SearchQuery searchQuery = new SearchQuery();
        searchQuery.setSeasonNumber(seasonNumber);
        searchQuery.setEpisodeNumber(episodeNumber);
        searchQuery.setKeywords(keywords);
        return this.getProgramsBySearchQuery(searchQuery);
    }

    @Override
    public List<Program> getProgramsByTitleAndChannel(String programTitle,
                                                      String channel) {
        return tvDao.getProgramsByTitleAndChannel(programTitle, channel);
    }

    @Override
    public List<Channel> getChannels() {
        return tvDao.getChannels();
    }

    @Override
    public List<Program> getProgramsByChannel(Channel channel) {
        return tvDao.getProgramsByChannel(channel);
    }

}
