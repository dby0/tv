/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.comms.IOHelper;
import com.webstersmalley.tv.db.SearchQueryDao;
import com.webstersmalley.tv.domain.SearchQuery;

import java.io.InputStream;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 21/01/13
 */
public class SearchQueryLoaderService {
    private String filename;
    private InputStream is;
    private SearchQueryDao searchQueryDao;

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setIs(InputStream is) {
        this.is = is;
    }

    public void setSearchQueryDao(SearchQueryDao searchQueryDao) {
        this.searchQueryDao = searchQueryDao;
    }

    public void loadSearchQueries() {
        List<String> contents;
        if (is == null) {
            contents = IOHelper.getLines(filename);
        } else {
            contents = IOHelper.getLines(is);
        }

        for (String query : contents) {
            SearchQuery searchQuery = SearchQuery.fromString(query);
            searchQueryDao.saveSearchQuery(searchQuery);
        }
    }
}
