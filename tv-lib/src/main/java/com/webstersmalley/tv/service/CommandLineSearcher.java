/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.db.SearchQueryDao;
import com.webstersmalley.tv.db.TvDao;
import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.domain.SearchQuery;
import com.webstersmalley.tv.sky.SkyResolverService;
import com.webstersmalley.tv.sky.SkyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by: Matthew Smalley
 * Date: 23/09/12
 */
@Service("commandLineSearcher")
public class CommandLineSearcher {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Resource(name = "listerService")
    private ListerService listerService;

    @Resource(name = "listingsRefresher")
    private ListingsRefresher listingsRefresher;

    @Resource(name = "searchQueryDao")
    private SearchQueryDao searchQueryDao;

    @Resource(name = "searchQueryLoaderService")
    private SearchQueryLoaderService searchQueryLoaderService;

    @Resource(name = "skyService")
    private SkyService skyService;

    @Resource(name = "tvDao")
    private TvDao tvDao;

    @Resource(name = "skyResolverService")
    private SkyResolverService skyResolverService;

    @Value("${movie.minimum.rating}")
    private BigDecimal minimumRating;

    public void go() {
        long startTime = System.currentTimeMillis();
        searchQueryLoaderService.loadSearchQueries();

        ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                listingsRefresher.refreshListings();
            }
        });
        executor.execute(new Runnable() {
            @Override
            public void run() {
                skyResolverService.getSkyURL();
            }
        });
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException("Error waiting for tasks to complete: " + e.getMessage(), e);
        }
        try {
            skyService.importRecordings();
        } catch (Exception e) {
            logger.error("Error getting recordings, continuing: " + e.getMessage(), e);
        }
        List<SearchQuery> searchQueries = searchQueryDao.getSearchQueries();
        Collections.sort(searchQueries);
        Map<SearchQuery, List<Program>> map = new HashMap<SearchQuery, List<Program>>();


        long sqStart = System.currentTimeMillis();
        for (SearchQuery searchQuery : searchQueries) {
            List<Program> matchedPrograms = listerService.getProgramsBySearchQuery(searchQuery);
            map.put(searchQuery, matchedPrograms);
        }
        logger.info("Time taken just for search queries: {} ms", System.currentTimeMillis() - sqStart);
        logger.info("Time taken just for db queries: {} ms", tvDao.getQueryTime());

        List<Program> movies = tvDao.getMovies(minimumRating);

        List<Program> summaries = tvDao.getProgramSummaries();

        searchResultsHandler.handleResults(map, movies, summaries);
        long endTime = System.currentTimeMillis();
        logger.info("Time taken {} ms", endTime - startTime);
    }

    @Resource(name = "searchResultsHandler")
    private SearchResultsHandler searchResultsHandler;

    public static void main(String[] args) {
        if (System.getProperty("environment") == null) {
            System.setProperty("environment", "dev");
        }
        if (System.getProperty("target") == null) {
            System.setProperty("target", "file");
        }
        ApplicationContext ac = new ClassPathXmlApplicationContext("environment.xml");
        CommandLineSearcher commandLineSearcher = ac.getBean("commandLineSearcher", CommandLineSearcher.class);
        commandLineSearcher.go();

        //TableDumper dumper = ac.getBean("tableDumper", TableDumper.class);
        //dumper.dumpTable("select * from vprograms");
    }

}
