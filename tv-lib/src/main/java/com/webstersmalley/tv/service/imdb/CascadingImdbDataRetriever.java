package com.webstersmalley.tv.service.imdb;

import com.webstersmalley.tv.domain.Program;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by: Matthew Smalley
 * Date: 18/05/13
 */
@Service("imdbDataRetriever")
public class CascadingImdbDataRetriever implements ImdbDataRetriever {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource(name = "imdbApiImdbDataRetriever")
    private ImdbDataRetriever imdbApiImdbDataRetriever;

    @Resource(name = "omdbApiImdbDataRetriever")
    private ImdbDataRetriever omdbApiImdbDataRetriever;

    @Resource(name = "deanClatworthyImdbDataRetriever")
    private ImdbDataRetriever deanclatworthyImdbDataRetriever;

    @Override
    public void addImdbData(Program program) {

        try {
            if (program.getImdbId() == null) {
                deanclatworthyImdbDataRetriever.addImdbData(program);
            }
        } catch (Exception e) {
            logger.info("Error with retriever, trying next");
        }

        try {
            if (program.getImdbId() == null) {
                imdbApiImdbDataRetriever.addImdbData(program);
            }
        } catch (Exception e) {
            logger.info("Error with retriever, trying next");
        }

        try {
            if (program.getImdbId() == null) {
                omdbApiImdbDataRetriever.addImdbData(program);
            }
        } catch (Exception e) {
            logger.info("Error with retriever, trying next");
        }

    }
}
