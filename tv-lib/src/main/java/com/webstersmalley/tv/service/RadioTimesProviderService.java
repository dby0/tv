/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.comms.Comms;
import com.webstersmalley.tv.domain.Channel;
import com.webstersmalley.tv.domain.Program;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("listingsProviderService")
public class RadioTimesProviderService implements ListingsProviderService {

    private final static String CHANNELS_URL = "http://xmltv.radiotimes.com/xmltv/channels.dat";
    private final static String LISTINGS_URL_STUB = "http://xmltv.radiotimes.com/xmltv/";

    private final DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
    @Resource(name = "comms")
    private Comms comms;

    public String getChannelListAsString() {
        return comms.getURLAsString(CHANNELS_URL);
    }

    public void setComms(Comms comms) {
        this.comms = comms;
    }

    @Override
    public List<Channel> getChannels() {
        List<Channel> channels = new ArrayList<Channel>();
        String fullChannelsString = getChannelListAsString();
        String[] channelStrings = fullChannelsString.split("\n");
        for (String channelString : channelStrings) {
            if (channelString.indexOf("|") > -1) {
                String[] parts = channelString.split("\\|");
                Channel channel = new Channel();
                channel.setId(Integer.parseInt(parts[0].trim()));
                channel.setName(parts[1].trim());
                channels.add(channel);
            }
        }

        return channels;
    }

    protected void parseEpisodeNumber(String[] bits, Program program) {
        Matcher matcher =
                Pattern.compile("series [0-9]+").matcher(bits[1]);

        if (matcher.find()) {
            String season = matcher.group().replace("series ", "");
            program.setSeasonNumber(season);
        }

        matcher = Pattern.compile("^[0-9]+").matcher(bits[1]);
        if (matcher.find()) {
            program.setEpisodeNumber(matcher.group());
        }
    }

    @Override
    public Set<Program> getProgramsForChannel(Channel channel) {
        Set<Program> programs = new HashSet<Program>();
        String fullList = comms.getURLAsString(LISTINGS_URL_STUB + channel.getId() + ".dat");
        if (fullList != null) {
            String[] lines = fullList.split("\n");
            for (int i = 0; i < lines.length; i++) {
                String line = lines[i].trim();
                if (line.indexOf("~") > -1) {
                    String[] bits = line.split("~");
                    while (bits.length < 23 && i < lines.length - 1) {
                        line = line + lines[i + 1].trim();
                        bits = line.split("~");
                        i++;
                    }
                    Program program = new Program();
                    program.setTitle(bits[0]);
                    parseEpisodeNumber(bits, program);
                    program.setSubtitle(bits[2]);
                    program.setYear(bits[3]);
                    program.setRepeat(Boolean.parseBoolean(bits[8]));
                    program.setNewSeries(Boolean.parseBoolean(bits[11]));
                    program.setDescription(bits[17]);
                    try {
                        program.setStartTime(dtf.parseDateTime(bits[19] + " " + bits[20]));
                    } catch (Exception e) {
                        throw new RuntimeException("Error parsing date string: " + bits[19] + " " + bits[20], e);
                    }
                    program.setDuration(Integer.parseInt(bits[22]));
                    program.setChannel(channel);
                    program.setMovie(Boolean.parseBoolean(bits[7]));
                    programs.add(program);
                }
            }
        }

        return programs;
    }
}
