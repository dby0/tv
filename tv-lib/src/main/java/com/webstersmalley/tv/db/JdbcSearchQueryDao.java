/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.db;

import com.webstersmalley.tv.domain.SearchQuery;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 23/09/12
 */
@Service("searchQueryDao")
public class JdbcSearchQueryDao implements SearchQueryDao {
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private JdbcTemplate jdbcTemplate;

    private final static String SAVE_SEARCH_SQL = "insert into searchQueries (seasonNumber, episodeNumber, keywords, titleSearch, isMovie) values (:seasonNumber, :episodeNumber, :keywords, :titleSearch, :isMovie)";
    private final static String GET_SEARCH_SQL = "select * from searchQueries";

    private static class SearchQueryRowMapper implements RowMapper<SearchQuery> {

        @Override
        public SearchQuery mapRow(ResultSet rs, int rowNum) throws SQLException {
            SearchQuery searchQuery = new SearchQuery();
            searchQuery.setId(rs.getInt("id"));
            searchQuery.setSeasonNumber(rs.getString("seasonNumber"));
            searchQuery.setEpisodeNumber(rs.getString("episodeNumber"));
            String keywordString = rs.getString("keywords");
            if (keywordString != null && !"".equals(keywordString.trim())) {
                String[] keywordsArray = keywordString.split(",");
                List<String> keywords = new ArrayList<String>();
                for (String keyword : keywordsArray) {
                    if (keyword != null) {
                        keywords.add(keyword);
                    }
                }
                searchQuery.setKeywords(keywords);
            }
            searchQuery.setTitleSearch(rs.getString("titleSearch"));
            searchQuery.setMovie(rs.getBoolean("isMovie"));
            return searchQuery;
        }
    }

    @Resource(name = "dataSource")
    public void setDataSource(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void saveSearchQuery(SearchQuery searchQuery) {
        searchQuery.validate();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("seasonNumber", searchQuery.getSeasonNumber());
        params.addValue("episodeNumber", searchQuery.getEpisodeNumber());
        StringBuilder sb = new StringBuilder();
        if (searchQuery.getKeywords() != null) {
            for (String keyword : searchQuery.getKeywords()) {
                sb.append(keyword).append(",");
            }
        }

        params.addValue("keywords", sb.toString());
        params.addValue("titleSearch", searchQuery.getTitleSearch());
        params.addValue("isMovie", searchQuery.isMovie());
        namedParameterJdbcTemplate.update(SAVE_SEARCH_SQL, params);
    }

    @Override
    public List<SearchQuery> getSearchQueries() {
        return jdbcTemplate.query(GET_SEARCH_SQL, new SearchQueryRowMapper());
    }
}
