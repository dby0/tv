/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.comms;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 28/04/13
 */
public class IOHelper {
    private IOHelper() {

    }

    public static List<String> getLines(String resourcePath) {
        return getLines(getInputStream(resourcePath));
    }

    public static List<String> getLines(InputStream is) {
        try {
            return IOUtils.readLines(is);
        } catch (IOException e) {
            throw new RuntimeException("Error loading resource: " + e.getMessage(), e);
        }
    }

    public static InputStream getInputStream(String resourcePath) {
        try {
            if (resourcePath.startsWith("classpath:")) {
                return IOHelper.class.getResourceAsStream("/" + resourcePath.replace("classpath:", ""));
            } else {
                return new FileInputStream(resourcePath);
            }
        } catch (IOException e) {
            throw new RuntimeException("Error loading resource: " + e.getMessage(), e);
        }
    }

    public static String getResource(String resourcePath) {
        if (resourcePath == null) {
            return null;
        }
        try {
            return IOUtils.toString(getInputStream(resourcePath));
        } catch (IOException e) {
            throw new RuntimeException("Error loading resource: " + e.getMessage(), e);
        }
    }
}
