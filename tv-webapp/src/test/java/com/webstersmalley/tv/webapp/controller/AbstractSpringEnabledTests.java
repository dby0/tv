/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.webapp.controller;

import org.junit.BeforeClass;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

/**
 * Created by: Matthew Smalley
 * Date: 11/04/12
 */
@ContextConfiguration(locations = {"classpath:service-webcontrollers.xml"})
public abstract class AbstractSpringEnabledTests extends AbstractJUnit4SpringContextTests {
    @BeforeClass
    public static void setup() {
        System.setProperty("environment", "dev");
        System.setProperty("target", "stdout");
    }


}
