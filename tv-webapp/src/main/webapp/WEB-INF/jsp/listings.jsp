<%@ include file="/WEB-INF/jsp/include.jsp" %>
<html>
    <head>
        <title>TV</title>
        <link href="css/stylesheet.css" rel="stylesheet" type="text/css"/>
        <link href="css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-1.7.2.min.js"></script>
        <script src="js/jquery-ui-1.8.21.custom.min.js"></script>
    </head>
    <body>
        <div id="container">
            <div id="header"><a href="<c:url value='/'/>">TV</a></div>
            <div id="tabs-top">
                <ul>
                    <c:forEach items="${channels}" var="channel">
                        <li><a style="font-size: 10px;" href="#tabs-${channel.id}">${channel.name}</a></li>
                    </c:forEach>
                </ul>
                <c:forEach items="${channels}" var="channel">
                    <div id="tabs-${channel.id}">
                        <ul>
                            <c:forEach items="${dates}" var="date">
                                <li><a style="font-size: 10px;" href="#tabs-<joda:format value='${date}' pattern='yyyyMMdd'/>"><joda:format value='${date}' pattern="EEEEE dd-MMM"/></a></li>
                            </c:forEach>
                        </ul>
                        <c:forEach items="${dates}" var="date">
                            <div id="tabs-<joda:format value='${date}' pattern='yyyyMMdd'/>">
                                <table width="800px">
                                    <c:forEach items="${channelMappedPrograms[channel][date]}" var="program">
                                        <tr valign="top">
                                            <td><span class="programTime"><joda:format value="${program.startTime}" style="-S" /></span></td>
                                            <td><span class="programTitle">${program.title}</span>&nbsp;<span class="programEpisode">${program.episodeNumberText}</span>&nbsp;<span class="programSubtitle">${program.subtitle}</span>&nbsp;<span class="programDescription">${program.description}</span></td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </c:forEach>
                    </div>
                    <script>
                        $(function() {
                            $( "#tabs-${channel.id}" ).tabs();
                        });
                    </script>
                </c:forEach>
            </div>
        </div>
    </body>
    <script>
        $(function() {
            $( "#tabs-top" ).tabs();
        });
    </script>
</html>
