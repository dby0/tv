<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<title>TV</title>
</head>
<body>
<h1>Channel List</h1>

	<c:forEach items="${channelList}" var="channel">
		<a href="showListings.html?channelId=${channel.id}">${channel.name}</a>
		<br />
	</c:forEach>

</body>
</html>