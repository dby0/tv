# TV

TV is a project to consume tv listings, search against a stored set of queries, and report either in a gui or via email
on matches.

The genesis of this projects was when we missed one too many shows particularly on the BBC who tend to have short
serials (3-5 episodes), aren't repeated, and trailed WELL in advance of actual showing (ie so far in advance that you
"forget" what's coming).

A further embellishment is to communicate with a connected Sky+ box (DVR compatible with BSkyB's UK/ROI service), and
annotate search results with a tag saying whether it's setup for record.

## Building

Assuming you have maven installed (if not go to [maven][1] and download), simply call "mvn clean install"
in the root of the source tree.

## Common setup

prodsite.properties (copy from default.properties and make your own)
prodchannels.txt (list of channels you want to subscribe to - choose from [channels.dat][2])
searchqueries.txt (list of search queries - see below for format)

## Running tv-lib

tv-lib will download listings, perform a search and output to one of stdout, a file or email.

To run:

    :::shell
        java -cp path/to/tv-lib-version.jar -Denvironment=prod -Dtarget=email com.webstersmalley.tv.service.CommandLineSearcher 2> tv.err 1> tv.out

Switching "email" to "file" or "stdout" will redirect the output (though note there is a logger active on stdout as
well, so best to output to a file if you want to capture it).

## Running tv-webapp

tv-webapp is a web application (jee / war file) with a graphical user interface for downloading program listings,
performing ad-hoc searches / browsing channels etc.

To run, make sure you have a newish version of [jetty][3], and execute:

    :::shell
        java -Denvironment=prod -Dtarget=file -jar jetty-runner-9.0.0.v20130308.jar --port 9090 tv-webapp.war &

## Search query format

A simple text string will search for that string in the title of all downloaded programs.
eg PROGRAM will match both "THE PROGRAM" and "PROGRAM".

Adding a comma and a number will search for season/series number >= that number.
eg PROGRAM,5 will match "THE PROGRAM" season 7, but not season 1.

Adding quotes around the string will perform an exact match.

## Notes and disclaimers

This is a non profit / hobby project not affiliated with Radio Times, BBC, Sky, Atlas metabroadcast or any other
company or service offering. The project is here for educational purposes only, and the source code freely
available, subject to an Apache 2.0 license. The running code is intended for personal non-project use only, and
the tv listings are in no way redistributed.

Furthermore, the Sky+ box integration was achieved through guesswork, and is not endorsed by BSkyB or anyone else in the
know. We don't know the long term effect of running this query against the Sky box, so if you're worried, please don't
run it!

[1]: http://maven.apache.org
[2]: http://xmltv.radiotimes.com/xmltv/channels.dat
[3]: http://www.eclipse.org/jetty/
